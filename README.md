# TOMEO Project readme

To the run the tomeo project, run it with a single argument, which is a path to a folder containing videos.

For each video, there must be a `.png` file with the same name as it, in the same folder.
This image should have a 16:9 aspect ration, and will be the thumbnail for that video.

The videos must use the `.wmv` file format on windows or `.mp4` or `.mov` file formats on other platforms.

Videos may be within one subfolder of the root. For example, using the example videos provided with the project brief, a suitable folder layout would be:

```
VIDEOS
|   c.png
|   c.wmv
|   e.png
|   e.wmv
|
+---inside
|       b.png
|       b.wmv
|
\---outside
        a.png
        a.wmv
        d.png
        d.wmv
        f.png
        f.wmv
```

This layout is excluding the files for non-windows platforms, that is each `.mov` and `.mp4` file.