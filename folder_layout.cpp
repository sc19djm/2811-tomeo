#include "folder_layout.h"

static const int kPadding = 4;

void FolderData::minimiseClicked() {
    minimised = !minimised;
    if (minimised) {
        minimise->setText("Maximise");
    } else {
        minimise->setText("Minimise");
    }
    emit(minimiseChanged());
}

FolderData::FolderData(QVector<VideoButton *> videos, QLabel *header, QPushButton *minimise)
    : videos(videos), header(header), minimise(minimise) {
    connect(minimise, &QPushButton::released, this, &FolderData::minimiseClicked);
}

FolderLayout::FolderLayout(FolderSet *videos, QWidget *parent) : QLayout(parent) {
    favourites =
        new FolderData(QVector<VideoButton *>(), header("Favourites"), minimise_button());
    connect(favourites, &FolderData::minimiseChanged, this, &FolderLayout::folderMinimised);
    root_folder =
        new FolderData(add(videos->root_videos), header("Unsorted"), minimise_button());
    connect(root_folder, &FolderData::minimiseChanged, this, &FolderLayout::folderMinimised);
    for (auto &&folder : videos->folders.keys()) {
        FolderData *f =
            new FolderData(add(videos->folders[folder]), header(folder), minimise_button());
        connect(f, &FolderData::minimiseChanged, this, &FolderLayout::folderMinimised);
        folders.append(f);
    }
}

QVector<VideoButton *> FolderLayout::add(QVector<Video *> videos) {
    QVector<VideoButton *> result;
    for (auto &&vid : videos) {
        result.append(process(vid));
        if (vid->is_favourite()) {
            favouriteChanged(vid);
        }
        connect(vid, &Video::favouriteChanged, this, [vid, this]() { favouriteChanged(vid); });
    }
    return result;
}

void FolderLayout::favouriteChanged(Video *vid) {
    if (vid->is_favourite() && !favourite_buttons.contains(vid)) {
        VideoButton *btn = process(vid);
        favourite_buttons[vid] = btn;
        favourites->videos.append(btn);
    } else if (!vid->is_favourite() && favourite_buttons.contains(vid)) {
        favourites->videos.removeAll(favourite_buttons[vid]);
        delete favourite_buttons.take(vid);
    }
    activate();
}

VideoButton *FolderLayout::process(Video *vid) {
    VideoButton *result = new VideoButton;
    result->setVideo(vid);
    connect(result, &VideoButton::selectVideo, this, &FolderLayout::jumpTo);
    addWidget(result);
    return result;
}

QLabel *FolderLayout::header(QString text) {
    QLabel *header = new QLabel(text);
    QFont font;
    font.setPointSize(30);
    font.setBold(true);
    header->setFont(font);
    addWidget(header);
    return header;
}

QPushButton *FolderLayout::minimise_button() {
    QPushButton *btn = new QPushButton("Minimise");
    addWidget(btn);
    return btn;
}

// Implementation based on <https://doc.qt.io/qt-5/qtwidgets-layouts-flowlayout-example.html>

bool FolderLayout::hasHeightForWidth() const { return true; }

int FolderLayout::heightForWidth(int width) const {
    int height = doLayout(QRect(0, 0, width, 0), true);
    return height;
}

void FolderLayout::setGeometry(const QRect &rect) {
    QLayout::setGeometry(rect);
    doLayout(rect, false);
};

Qt::Orientations FolderLayout::expandingDirections() const { return {}; }

int FolderLayout::doLayout(const QRect &rect, bool testOnly) const {
    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);
    QRect effectiveRect = rect.adjusted(+left, +top, -right, -bottom);
    int x = effectiveRect.x();
    int y = effectiveRect.y();
    int width = effectiveRect.width();
    y = layout_folder(x, y, width, favourites, testOnly);
    y = layout_folder(x, y, width, root_folder, testOnly);
    for (auto &&folder : folders) {
        y = layout_folder(x, y, width, folder, testOnly);
    }
    return y;
}

int FolderLayout::layout_folder(int x, int y, int width, FolderData *folder,
                                bool testOnly) const {
    // Padding above and to the sides has already been handled.
    if (folder->videos.length() > 0) {
        QSize minHint = folder->minimise->sizeHint();
        int headerheight = folder->header->heightForWidth(width - minHint.width() - kPadding);
        if (!testOnly) {
            folder->header->setGeometry(x, y, width - minHint.width() - kPadding,
                                        headerheight);
            folder->minimise->setGeometry(x + width - minHint.width(), y, minHint.width(),
                                          minHint.height());
        }
        y += std::max(minHint.height(), headerheight) + kPadding;

        if (!folder->minimised) {
            int lineheight = 0;
            // First item doesn't need padding on the left, account for that.
            int availableSpace = width + kPadding;
            int linestart = 0;
            for (int i = 0; i < folder->videos.length(); i++) {
                VideoButton *it = folder->videos[i];
                QSize itemSizeHint = it->sizeHint();
                if (itemSizeHint.width() + kPadding > availableSpace) {
                    if (linestart == i) {
                        // Avoid infinite loop when there isn't enough width
                        if (!testOnly) {
                            it->setGeometry(x, y, width, itemSizeHint.height());
                        }
                        lineheight = itemSizeHint.height();
                        linestart = i + 1;
                    } else {
                        int xi = x;
                        for (int j = linestart; j < i; j++) {
                            // Lay out every element which fit in this line
                            // excluding this element
                            VideoButton *subIt = folder->videos[j];
                            QSize subSizeHint = subIt->sizeHint();
                            if (!testOnly) {
                                subIt->setGeometry(xi, y, subSizeHint.width(), lineheight);
                            }
                            xi += subSizeHint.width() + kPadding;
                        }
                        // Handle this item in the next line
                        i -= 1;
                    }
                    linestart = i + 1;
                    y += lineheight + kPadding;
                    lineheight = 0;
                    availableSpace = width + kPadding;
                } else {
                    availableSpace -= itemSizeHint.width() + kPadding;
                    if (itemSizeHint.height() > lineheight) {
                        lineheight = itemSizeHint.height();
                    }
                }
            }
            int xi = x;
            for (int j = linestart; j < folder->videos.length(); j++) {
                // Lay out every element which fit in this line
                // excluding this element
                VideoButton *subIt = folder->videos[j];
                QSize subSizeHint = subIt->sizeHint();
                if (!testOnly) {
                    subIt->setGeometry(xi, y, subSizeHint.width(), lineheight);
                }
                QRect geo = subIt->geometry();
                xi += subSizeHint.width() + kPadding;
            }
            y += lineheight + kPadding;
        } else {
            for (auto &&f : folder->videos) {
                f->setGeometry(-1, -1, 1, 1);
            }
        }
    } else {
        if (!testOnly) {
            folder->header->setGeometry(-1, -1, 1, 1);
            folder->minimise->setGeometry(-1, -1, 1, 1);
        }
    }
    return y;
}

QLayoutItem *FolderLayout::itemAt(int index) const { return items.value(index); };
QLayoutItem *FolderLayout::takeAt(int index) {
    if (index >= 0 && index < items.size())
        return items.takeAt(index);
    return nullptr;
};
void FolderLayout::addItem(QLayoutItem *item) { items.append(item); };
int FolderLayout::count() const { return items.size(); };

FolderLayout::~FolderLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0))) {
        delete item;
    }
    delete root_folder;
    delete favourites;
    for (auto &&folder : folders) {
        delete folder;
    }
}