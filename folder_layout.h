#ifndef CW3_FOLDER_LAYOUT_H
#define CW3_FOLDER_LAYOUT_H

#include "folder_set.h"
#include "video_button.h"

#include <QLabel>
#include <QLayout>
#include <QMap>
#include <QVector>

class FolderData : public QObject {
    Q_OBJECT
  public:
    FolderData(QVector<VideoButton *> videos, QLabel *header, QPushButton *minimise);

    QVector<VideoButton *> videos;
    QLabel *header;
    QPushButton *minimise;
    bool minimised = false;
  signals:
    void minimiseChanged();
  private slots:
    void minimiseClicked();
};

class FolderLayout : public QLayout {
    Q_OBJECT
  public:
    FolderLayout(FolderSet *videos, QWidget *parent = nullptr);
    ~FolderLayout();
    Qt::Orientations expandingDirections() const override;
    bool hasHeightForWidth() const override;
    int heightForWidth(int) const override;

    void setGeometry(const QRect &rect) override;
    QSize minimumSize() const override { return QSize(0, 0); };
    QSize sizeHint() const override { return QSize(600, heightForWidth(600)); };

    QLayoutItem *itemAt(int index) const override;
    QLayoutItem *takeAt(int index) override;
    void addItem(QLayoutItem *item) override;
    int count() const override;

  signals:
    void jumpTo(Video *vid);

  private slots:
    void favouriteChanged(Video *vid);
    void folderMinimised() { activate(); };

  private:
    FolderData *root_folder;
    QVector<FolderData *> folders;
    FolderData *favourites;
    QMap<Video *, VideoButton *> favourite_buttons;

    QVector<QLayoutItem *> items;

    QVector<VideoButton *> add(QVector<Video *> videos);
    VideoButton *process(Video *vid);
    QLabel *header(QString text);
    QPushButton *minimise_button();

    int doLayout(const QRect &rect, bool testOnly) const;
    int layout_folder(int x, int y, int width, FolderData *folder, bool testOnly) const;
};

#endif
