#include "folder_set.h"

#include <QDir>
#include <QDirIterator>
#include <QImageReader>
#include <QVector>

QVector<Video *> FolderSet::videos_in(QString folder) {

    // read in videos and thumbnails to this directory
    QVector<Video *> out;
    QDir dir(folder);
    dir.setFilter(QDir::Files);
    QDirIterator it(dir);
    while (it.hasNext()) { // for all files

        QString f = it.next();
        QFileInfo info(f);
        QString suffix = info.suffix().toLower();
#if defined(_WIN32)
        // We use wmv files, because windows definitely has this codec installed. No need to
        // support anything fancier on windows
        if (suffix == "wmv") {
#else
        if (suffix == "mp4" || suffix == "mov") { // mac/linux
#endif

            QString thumb = dir.filePath(info.completeBaseName() + ".png");
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);

                QImage sprite = imageReader->read(); // read the thumbnail
                if (!sprite.isNull()) {
                    QIcon *ico = new QIcon(
                        QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                    QUrl *url = new QUrl(
                        QUrl::fromLocalFile(f)); // convert the file location to a generic url

                    Video *new_vid = new Video(url, ico, QFileInfo(f).fileName());
                    connect(new_vid, &Video::favouriteChanged, this,
                            &FolderSet::favouriteUpdated);
                    out.push_back(new_vid); // add to the output list
                } else
                    qDebug() << "warning: skipping video because I couldn't process "
                                "thumbnail "
                             << thumb << endl;
            } else
                qDebug() << "warning: skipping video because I couldn't find thumbnail "
                         << thumb << endl;
        }
    }

    return out;
}

void FolderSet::resolve() {
    QDir root(rootPath);
    root.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QDirIterator it(root);
    while (it.hasNext()) {
        QString f = it.next();
        QVector<Video *> folder = videos_in(f);
        QFileInfo info(f);
        folders.insert(info.fileName(), folder);
        count_ += folder.size();
    }
    root_videos = videos_in(rootPath);
    count_ += root_videos.size();
}

Video *FolderSet::arbitrary() {
    if (root_videos.size() > 0) {
        return root_videos[0];
    }
    for (auto &&i : folders) {
        if (i.size() > 0) {
            return i[0];
        }
    }
    return nullptr;
}

QVector<Video *> FolderSet::favourites() {
    QVector<Video *> result;
    for (auto &&root_vid : root_videos) {
        if (root_vid->is_favourite()) {
            result.append(root_vid);
        }
    }
    for (auto &&folder : folders) {
        for (auto &&vid : folder) {
            if (vid->is_favourite()) {
                result.append(vid);
            }
        }
    }

    return result;
}