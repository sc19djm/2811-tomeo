#ifndef CW3_FOLDER_SET_H
#define CW3_FOLDER_SET_H

#include <QDir>
#include <QIcon>
#include <QMap>
#include <QString>
#include <QUrl>
#include <QVector>

class Video : public QObject {
    Q_OBJECT
  public:
    const QUrl *url;   // video file to play
    const QIcon *icon; // icon to display
    const QString title;

    bool is_favourite() { return favourited; }

    Video(QUrl *url, QIcon *icon, QString title)
        : url(url), icon(icon), title(title), QObject() {}

  public slots:
    void activate() { emit(jumpTo()); };
    void toggleFavourite() {
        favourited = !favourited;
        emit(favouriteChanged());
    };

  signals:
    void favouriteChanged();
    void jumpTo();

  private:
    bool favourited = false;
};

class FolderSet : public QObject {
    Q_OBJECT
  public:
    QVector<Video *> root_videos;
    QMap<QString, QVector<Video *>> folders;
    FolderSet() : QObject(){};
    FolderSet(QString root) : rootPath(root){};
    void resolve();
    void setRoot(QString root) { rootPath = root; }
    int count() { return count_; };
    Video *arbitrary();
    QVector<Video *> favourites();
  signals:
    void favouriteUpdated();

  private:
    int count_ = 0;
    QString rootPath;
    QVector<Video *> videos_in(QString folder);
};

#endif