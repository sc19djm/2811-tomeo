/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include "video_organiser.h"

#include <QApplication>
#include <QDesktopServices>
#include <QImageReader>
#include <QMediaPlaylist>
#include <QMessageBox>
#include <QScrollArea>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtCore/QFileInfo>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QtWidgets/QFileIconProvider>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << Qt::endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // collect all the videos in the folder
    FolderSet *videos = new FolderSet;

    if (argc == 2) {

        videos->setRoot(argv[1]);
        videos->resolve();
    }

    if (videos->count() == 0) {

        const int result = QMessageBox::question(
            NULL, QString("Tomeo"),
            QString("no videos found! download, unzip, and add command line argument to "
                    "\"quoted\" file location. Download videos from Tom's OneDrive?"),
            QMessageBox::Yes | QMessageBox::No);

        switch (result) {
        case QMessageBox::Yes:
            QDesktopServices::openUrl(
                QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/"
                     "EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
            break;
        default:
            break;
        }
        exit(-1);
    }

    VideoOrganiser window(videos);

    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 680);

    // showtime!
    window.show();

    // wait for the app to terminate
    return app.exec();
}
