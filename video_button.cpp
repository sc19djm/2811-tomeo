#include "video_button.h"

#include <QSizePolicy>

// The size of the icon in the buttons
static const QSize kIconSize(176, 99);

VideoButton::VideoButton(QWidget *parent) : QPushButton(parent) {
    favourite_button = new QPushButton;
    icon_label = new QLabel;
    icon_label->setTextInteractionFlags(Qt::NoTextInteraction);

    text_label = new QLabel;
    text_label->setTextInteractionFlags(Qt::NoTextInteraction);

    favourite_button->setFixedSize(QSize(25, 25));
    QFont favourite_font;
    favourite_font.setPixelSize(18);
    favourite_button->setFont(favourite_font);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(icon_label, 0, 0, 1, 2);
    layout->addWidget(text_label, 1, 0);
    layout->addWidget(favourite_button, 1, 1);

    setLayout(layout);
}

void VideoButton::setVideo(Video *i) {
    if (vid) {
        disconnect(vid, &Video::favouriteChanged, this, &VideoButton::handleVideoFavourite);
        disconnect(favourite_button, &QPushButton::released, vid, &Video::toggleFavourite);
    } else {
        connect(this, &QPushButton::released, this, &VideoButton::clicked);
    }

    vid = i;
    icon_label->setPixmap(vid->icon->pixmap(kIconSize));
    icon_label->setFixedSize(kIconSize);
    text_label->setText(vid->title);
    handleVideoFavourite();

    connect(vid, &Video::favouriteChanged, this, &VideoButton::handleVideoFavourite);
    connect(favourite_button, &QPushButton::released, vid, &Video::toggleFavourite);
}

void VideoButton::handleVideoFavourite() {
    if (vid->is_favourite()) {
        favourite_button->setText("-");
    } else {
        favourite_button->setText("+");
    }
}