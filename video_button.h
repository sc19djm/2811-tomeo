#ifndef CW2_VIDEO_BUTTON_H
#define CW2_VIDEO_BUTTON_H

#include "folder_set.h"

#include <QGridLayout>
#include <QLabel>
#include <QMouseEvent>
#include <QPicture>
#include <QPushButton>
#include <QToolButton>
#include <QUrl>

class VideoButton : public QPushButton {
    Q_OBJECT

  public:
    VideoButton(QWidget *parent = nullptr);

    void setVideo(Video *i);
    QSize sizeHint() const override { return this->layout()->sizeHint(); };

  signals:
    void selectVideo(Video *vid);

  private slots:
    void handleVideoFavourite();
    void clicked() { emit(selectVideo(vid)); };

  private:
    Video *vid = nullptr;
    QPushButton *favourite_button;
    QLabel *icon_label;
    QLabel *text_label;
};

#endif // CW2_THE_BUTTON_H
