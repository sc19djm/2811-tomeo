#include "video_organiser.h"

#include "folder_layout.h"
#include "folder_set.h"
#include "video_button.h"

#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QScrollArea>
#include <QSlider>
#include <QSplitter>
#include <QVideoWidget>
#include <QWidget>

// The number of milliseconds between updates of the video progress slider.
static const int kNotifyRate = 16;

// Create the half of the application containing the video area and the controls.
QVBoxLayout *VideoOrganiser::create_video_half() {
    playButton->setText("Pause");
    QVideoWidget *video_widget = new QVideoWidget;
    player->setVideoOutput(video_widget);
    // We use the new style signal/slot mechanism throughout this application. This improves
    // compilation error messages
    connect(player, &QMediaPlayer::stateChanged, this, &VideoOrganiser::playStateChanged);
    connect(player, &QMediaPlayer::positionChanged, this, &VideoOrganiser::positionChanged);

    connect(playButton, &QPushButton::released, this, &VideoOrganiser::playPressed);

    QSlider *volume = new QSlider;
    volume->setRange(0, 100);
    volume->connect(volume, &QAbstractSlider::valueChanged, player, &QMediaPlayer::setVolume);
    videoSlider = new QSlider;

    volume->setOrientation(Qt::Horizontal);
    videoSlider->setOrientation(Qt::Horizontal);

    QHBoxLayout *controls = new QHBoxLayout;
    controls->addWidget(playButton);
    controls->addWidget(volume);
    controls->addWidget(videoSlider, 1);

    // Video information be presented here, but it is not yet implemented
    QLabel *info = new QLabel("Video information not implemented");

    QVBoxLayout *video_area_layout = new QVBoxLayout;
    video_area_layout->addWidget(video_widget, 1);
    // Controls needs to be pinned to the bottom of the video widget, so don't give it a
    // stretch factor
    video_area_layout->addLayout(controls);
    video_area_layout->addWidget(info, 1);
    return video_area_layout;
}

VideoOrganiser::VideoOrganiser(FolderSet *vids, QWidget *parent)
    : QSplitter(parent), player(new QMediaPlayer(this)), videos(vids),
      playButton(new QPushButton) {

    FolderLayout *selector_panel = new FolderLayout(vids);
    connect(selector_panel, &FolderLayout::jumpTo, this, &VideoOrganiser::jumpTo);
    // The selector is implemented as a layout, so we need a widget for it, because QScrollArea
    // is a view over a widget
    QWidget *selectorWidget = new QWidget();
    selectorWidget->setLayout(selector_panel);

    QScrollArea *video_scroll_area = new QScrollArea;
    video_scroll_area->setWidget(selectorWidget);
    video_scroll_area->setWidgetResizable(true);
    // Add the widget to the splitter
    addWidget(video_scroll_area);

    player->setMedia(*vids->arbitrary()->url);
    player->play();
    player->setNotifyInterval(kNotifyRate);
    player->setVolume(0);

    QWidget *video_half_widget = new QWidget;
    video_half_widget->setLayout(create_video_half());
    addWidget(video_half_widget);
    setSizes(QList<int>{2000, 1000});
}

void VideoOrganiser::playStateChanged(QMediaPlayer::State ms) {
    // If the video stops (because it reached the end), then restart it
    if (ms == QMediaPlayer::State::StoppedState) {
        player->play();
    }
}

void VideoOrganiser::positionChanged(qint64 progress) {
    // Reset the range because if the video has changed, then the slider range will be
    // out-of-date.
    videoSlider->setRange(0, player->duration() / kNotifyRate);
    videoSlider->setValue(progress / kNotifyRate);
}

void VideoOrganiser::jumpTo(Video *button) {
    player->setMedia(*button->url);
    player->play();
};

// Handle the play button being pressed
void VideoOrganiser::playPressed() {
    if (paused) {
        player->play();
        playButton->setText("Pause");
    } else {
        player->pause();
        playButton->setText("Play");
    }
    paused = !paused;
}