#ifndef CW3_VIDEO_ORGANISER_H
#define CW3_VIDEO_ORGANISER_H

#include "folder_set.h"
#include "video_button.h"

#include <QGridLayout>
#include <QMediaPlayer>
#include <QPushButton>
#include <QSlider>
#include <QSplitter>

// We use a QSplitter here to allow resizing the screen
class VideoOrganiser : public QSplitter {
    Q_OBJECT
  public:
    VideoOrganiser(FolderSet *vids, QWidget *parent = nullptr);

  private:
    QMediaPlayer *player;
    FolderSet *videos;
    QSlider *videoSlider;
    QPushButton *playButton;
    // Whether the video is paused. This is used mainly to control the state of playButton
    bool paused = false;

    QVBoxLayout *create_video_half();
  private slots:
    void playPressed();
    void positionChanged(qint64 progress);
    void playStateChanged(QMediaPlayer::State ms);
    void jumpTo(Video *button);
};

#endif